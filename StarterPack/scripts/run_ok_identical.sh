#!/bin/sh
# Copyright (C) 2018-2020 |Meso|Star>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# run two commands and check result(s) identity
# success expected
if ! result1=$( $1 )
then
  echo "Error: Command failed!" >&2
  exit 1
fi
re='^[0-9 .eEgG+-]+$'
if ! echo "$result1" | grep -Eq "$re" ; then
  echo "Error: result does not start with a number: $result1" >&2
  echo "Error: don't use the option -e with stardis" >&2
  exit 1
fi

if ! result2=$( $2 )
then
  echo "Error: Command failed!" >&2
  exit 1
fi
re='^[0-9 .eEgG+-]+$'
if ! echo "$result2" | grep -Eq "$re" ; then
  echo "Error: result does not start with a number: $result2" >&2
  echo "Error: don't use the option -e with stardis" >&2
  exit 1
fi

echo "Result #1: $result1"
echo "Result #2: $result2"

if ! [ "$result1" = "$result2" ]
then
  echo "Error: results differ"
  exit 1
fi

# check that no sample failed for mc1 (last part of result: #failed #success)
count=$(echo "$result1" | wc -w)
idxfailed=$($count - 1)
failed=$(echo "$result1" | cut -d ' ' -f "$idxfailed")
if [ "$failed" -ne 0 ]
then
  echo "Error: $failed sample(s) failed" >&2
  exit 1
fi

# check that no sample failed for mc1 (last part of result: #failed #success)
count=$(echo "$result2" | wc -w)
idxfailed=$($count - 1)
failed=$(echo "$result2" | cut -d ' ' -f "$idxfailed")
if [ "$failed" -ne 0 ]
then
  echo "Error: $failed sample(s) failed" >&2
  exit 1
fi

echo "Message: Results are identical"
