static const char copyright_notice[] =
"Copyright (C) 2021-2022 |Meso|Star> (contact@meso-star.com).";

static const char license_short[] =
"Licensed under GPLv3, see <http://www.gnu.org/licenses/>.";

static const char license_text[] =
"This program is free software: you can redistribute it and/or modify\n\
it under the terms of the GNU General Public License as published by\n\
the Free Software Foundation, either version 3 of the License, or\n\
(at your option) any later version.\n\
This program is distributed in the hope that it will be useful,\n\
but WITHOUT ANY WARRANTY; without even the implied warranty of\n\
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the\n\
GNU General Public License for more details.\n\
You should have received a copy of the GNU General Public License\n\
along with this program. If not, see <http://www.gnu.org/licenses/>.";

/* Build:
 * gcc -shared -g -I ${stardis_path}/include -o libsolid.so -fPIC solid.c
 */

#include <stardis/stardis-prog-properties.h>

#include <float.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#define CHK_ARG(Name) \
  if(argc <= idx) {\
    fprintf(stderr,\
       "Solid '%s': invalid data (missing token '" Name "')\n",\
       solid->name);\
    goto error;\
  } else arg = argv[idx++]

struct my_solid {
  char name[128];
  double lambda, rho, cp;
  double delta;
  double tinit, imposed_temp;
  double vpower;
};

void
stardis_release_data
  (void* my_solid)
{
  assert(my_solid);
  free(my_solid);
}

void*
stardis_create_data
  (const struct stardis_description_create_context* ctx,
   void* data,
   size_t argc,
   char* argv[])
{
  struct my_solid* solid = NULL;
  char *arg, *end;
  size_t idx = 0;

  (void)data;

  if(!argv || !ctx) goto error;
  solid = malloc(sizeof(*solid));
  if(!solid) goto error;

  /* argv[0] = lambda */
  CHK_ARG("lambda");
  solid->lambda = strtod(arg, &end);
  if(solid->lambda <= 0 || arg == end) {
    fprintf(stderr, "Solid '%s': invalid lambda: %s\n", solid->name, arg);
    goto error;
  }
  /* argv[1] = rho */
  CHK_ARG("rho");
  solid->rho = strtod(arg, &end);
  if(solid->rho <= 0 || arg == end) {
    fprintf(stderr, "Solid '%s': invalid rho: %s\n", solid->name, arg);
    goto error;
  }
  /* argv[2] = cp */
  CHK_ARG("cp");
  solid->cp = strtod(arg, &end);
  if(solid->cp <= 0 || arg == end) {
    fprintf(stderr, "Solid '%s': invalid cp: %s\n", solid->name, arg);
    goto error;
  }
  /* argv[3] = delta */
  CHK_ARG("delta");
  solid->delta = strtod(arg, &end);
  if(solid->delta <= 0 || arg == end) {
    fprintf(stderr, "Solid '%s': invalid delta: %s\n", solid->name, arg);
    goto error;
  }
  /* argv[4] = initial temperature */
  CHK_ARG("initial temperature");
  solid->tinit = strtod(arg, &end);
  if(solid->tinit < 0 || arg == end) {
    fprintf(stderr, "Solid '%s': invalid initial temperature: %s\n",
       solid->name, arg);
    goto error;
  }
  /* argv[5] = imposed temperature */
  CHK_ARG("imposed temperature");
  if(0 == strcmp(arg, "UNKNOWN")) {
    solid->imposed_temp = -1;
  } else {
    solid->imposed_temp = strtod(arg, &end);
    if(solid->imposed_temp < 0 || arg == end) {
      fprintf(stderr, "Solid '%s': invalid imposed temperature: %s\n",
         solid->name, arg);
      goto error;
    }
  }
  /* argv[6] = volumic power */
  CHK_ARG("volumic power");
  solid->vpower = DBL_MAX; /* invalid */
  solid->vpower = strtod(arg, &end);
  if(solid->vpower == DBL_MAX || arg == end) {
    fprintf(stderr, "Solid '%s': invalid power: %s\n", solid->name, arg);
    goto error;
  }
  if(argc > idx) {
    fprintf(stderr, "Solid '%s': trailing arguments:\n", solid->name);
    for( ; idx < argc; idx++) fprintf(stderr, "%s\n", argv[idx]);
    goto error;
  }

end:
  return solid;
error:
  if(solid) stardis_release_data(solid);
  solid = NULL;
  goto end;
}

const char*
get_copyright_notice
  (void* data)
{
  (void)data;
  return copyright_notice;
}

const char*
get_license_short
  (void* data)
{
  (void)data;
  return license_short;
}

const char*
get_license_text
  (void* data)
{
  (void)data;
  return license_text;
}

double
stardis_conductivity
  (const struct stardis_vertex* vtx,
   void* data)
{
  struct my_solid* solid = data;
  (void)vtx;
  return solid->lambda;
}

double
stardis_volumic_mass
  (const struct stardis_vertex* vtx,
   void* data)
{
  struct my_solid* solid = data;
  (void)vtx;
  return solid->rho;
}

double
stardis_calorific_capacity
  (const struct stardis_vertex* vtx,
   void* data)
{
  struct my_solid* solid = data;
  (void)vtx;
  return solid->cp;
}

double
stardis_delta_solid
  (const struct stardis_vertex* vtx,
   void* data)
{
  struct my_solid* solid = data;
  (void)vtx;
  return solid->delta;
}

double
stardis_medium_temperature
  (const struct stardis_vertex* vtx,
   void* data)
{
  struct my_solid* solid = data;
  if(vtx->time <= 0) {
    return solid->tinit;
  }
  return solid->imposed_temp;
}

double
stardis_volumic_power
  (const struct stardis_vertex* vtx,
   void* data)
{
  struct my_solid* solid = data;
  (void)vtx;
  return solid->vpower;
}

double*
stardis_t_range
  (void* data,
   double range[2])
{
  struct my_solid* solid = data;
  range[0] = fmin(range[0], solid->tinit);
  range[1] = fmax(range[1], solid->tinit);
  return range;
}

