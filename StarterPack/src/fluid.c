static const char copyright_notice[] =
"Copyright (C) 2021-2022 |Meso|Star> (contact@meso-star.com).";

static const char license_short[] =
"Licensed under GPLv3, see <http://www.gnu.org/licenses/>.";

static const char license_text[] =
"This program is free software: you can redistribute it and/or modify\n\
it under the terms of the GNU General Public License as published by\n\
the Free Software Foundation, either version 3 of the License, or\n\
(at your option) any later version.\n\
This program is distributed in the hope that it will be useful,\n\
but WITHOUT ANY WARRANTY; without even the implied warranty of\n\
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the\n\
GNU General Public License for more details.\n\
You should have received a copy of the GNU General Public License\n\
along with this program. If not, see <http://www.gnu.org/licenses/>.";

/* Build:
 * gcc -shared -g -I ${stardis_path}/include -o libfluid.so -fPIC fluid.c
 */

#include <stardis/stardis-prog-properties.h>

#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#define CHK_ARG(Name) \
  if(argc <= idx) {\
    fprintf(stderr,\
       "Fluid '%s': invalid data (missing token '" Name "')\n",\
       fluid->name);\
    goto error;\
  } else arg = argv[idx++]

struct my_fluid {
  char name[128];
  double cp, rho;
  double tinit, imposed_temp;
};

void
stardis_release_data
  (void* my_fluid)
{
  assert(my_fluid);
  free(my_fluid);
}

void*
stardis_create_data
  (const struct stardis_description_create_context* ctx,
   void* data,
   size_t argc,
   char* argv[])
{
  struct my_fluid* fluid = NULL;
  char *arg, *end;
  size_t idx = 0;

  (void)data;

  if(!argv || ! ctx) goto error;
  fluid = malloc(sizeof(*fluid));
  if(!fluid) goto error;

  strncpy(fluid->name, ctx->name, 127);

  /* argv[0] = rho */
  CHK_ARG("rho");
  fluid->rho = strtod(arg, &end);
  if(fluid->rho <= 0 || arg == end) {
    fprintf(stderr, "Invalid Rho: %s\n", arg);
    goto error;
  }
  /* argv[1] = cp */
  CHK_ARG("cp");
  fluid->cp = strtod(arg, &end);
  if(fluid->cp <= 0 || arg == end) {
    fprintf(stderr, "Invalid Cp: %s\n", arg);
    goto error;
  }
  /* argv[2] = tinit */
  CHK_ARG("initial temperature");
  fluid->tinit = strtod(arg, &end);
  if(fluid->tinit < 0 || arg == end) {
    fprintf(stderr, "Invalid Initial temperature: %s\n", arg);
    goto error;
  }
  /* argv[3] = imposed temperature */
  CHK_ARG("imposed temperature");
  if(0 == strcmp(arg, "UNKNOWN")) {
    fluid->imposed_temp = -1;
  } else {
    fluid->imposed_temp = strtod(arg, &end);
    if(fluid->imposed_temp <= 0 || arg == end) {
      fprintf(stderr, "Invalid Imposed temperature: %s\n", arg);
      goto error;
    }
  }
  if(argc > idx) {
    fprintf(stderr, "Fluid '%s': trailing arguments:\n", fluid->name);
    for( ; idx < argc; idx++) fprintf(stderr, "%s\n", argv[idx]);
    goto error;
  }

  return fluid;
error:
  if(fluid) stardis_release_data(fluid);
  fluid = NULL;
  return NULL;
}

const char*
get_copyright_notice
  (void* data)
{
  (void)data;
  return copyright_notice;
}

const char*
get_license_short
  (void* data)
{
  (void)data;
  return license_short;
}

const char*
get_license_text
  (void* data)
{
  (void)data;
  return license_text;
}

double
stardis_calorific_capacity
  (const struct stardis_vertex* vtx,
   void* data)
{
  struct my_fluid* fluid = data;
  (void)vtx;
  return fluid->cp;
}

double
stardis_volumic_mass
  (const struct stardis_vertex* vtx,
   void* data)
{
  struct my_fluid* fluid = data;
  (void)vtx;
  return fluid->rho;
}

double
stardis_medium_temperature
  (const struct stardis_vertex* vtx,
   void* data)
{
  struct my_fluid* fluid = data;
  if(vtx->time <= 0) {
    return fluid->tinit;
  }
  return fluid->imposed_temp;
}

double*
stardis_t_range
  (void* data,
   double range[2])
{
  struct my_fluid* fluid = data;
  range[0] = fmin(range[0], fluid->tinit);
  range[1] = fmax(range[1], fluid->tinit);
  return range;
}

