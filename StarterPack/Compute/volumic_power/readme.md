Test test_sdis_volumic_power from stardis-solver project
========================================================

    /*
     * The scene is composed of a solid cube whose temperature is unknown.
     * The convection coefficient with the surrounding fluid is null everywhere.
     * The temperature of the +X and -X faces are fixed to T0, and the solid has a
     * volumic power of P0. This test computes the temperature of a probe position
     * pos into the solid and check that it is is equal to:
     *
     *    T(x,y,z) = P0 / (2*LAMBDA) * (A^2/4 - (x-0.5)^2) + T0
     *
     * with LAMBDA the conductivity of the solid and A=1 the size of cube.
     *
     *
     *       ///// (1,1,1)
     *       +-------+
     *      /'      /|
     *     +-------+ T0
     *    T0 +.....|.+
     *     |,      |/
     *     +-------+
     * (0,0,0) /////
     */


    $ stardis -V 2 -M material.txt -M bc.txt -p 0.25,0.25,0.25,inf
    329.223 0.0975229 0 10000

Expected T=329.375°K

    $ stardis -V 2 -M material.txt -M bc.txt -p 0.5,0.25,0.25 -e
    Temperature at [0.5, 0.25, 0.25] at t=inf = 332.306 K +/- 0.0987241
    #failures: 0/10000

Expected T=332.5°K
