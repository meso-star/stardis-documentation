Test of green functions
=======================

    /* Same as green_2 with a lot of lines in settings.txt to bench sgreen -a
     * performance
     */

    $ stardis -V 2 -M model.txt -p 0,0,0 -n 20000 -G probe.green
    warning: trace_radiative_path_3d: inconsistent medium definition at `-1 1 -0.907484'.
      
    $ time sgreen -g probe.green -a settings.txt > res_n.txt
    real    0m0,210s
    user    0m0,000s
    sys     0m0,015s
  
    $ time sgreen -g probe.green -a settings.txt -t 1 > res_1.txt
    real    0m0,659s
    user    0m0,000s
    sys     0m0,046s
    
    $ diff res_n.txt res_1.txt
