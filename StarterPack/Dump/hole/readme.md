Dump in the presence of no hole
===============================

    $ stardis -V 2 -M model.txt -d > no_hole.vtk

      
Field **Hole_frontiers** is not available.

Dump in the presence of a hole
==============================

    $ stardis -V 2 -M model_hole.txt -d > hole.vtk
    warning: Model contains hole(s).

      
Field **Hole_frontiers** is available. Possible values are:
- 0 : The triangle is not involved in any hole.
- 1 : At leat 1 edge of the triangle is part of the frontier of a hole.
