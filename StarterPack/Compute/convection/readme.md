Test test_convection from stardis-solver project
================================================

    /*
     * The scene is composed of an unit fluid cube whose temperature is
     * unknown. The convection coefficient with the surrounding solid is H
     * everywhere the temperature of the -/+X, -/+Y and -/+Z faces are fixed to T0
     * and T1, T2, T3, T4 and T5, respectively.  This test computes the temperature
     * of the fluid Tf at an observation time t. This temperature is equal to:
     *
     *    Tf(t) = Tf(0) * e^(-nu*t) + Tinf*(1-e^(-nu*t))
     *
     *    nu = (Sum_{i=0..5}(H*Si)) / (RHO*CP*V)
     *    Tinf = (Sum_{i=0..5}(H*Si*Ti) / (Sum_{i=0..5}(H*Si));
     *
     * with Si surface of the faces (i.e. one), V the volume of the cube (i.e.
     * one), RHO the volumic mass of the fluid and CP its calorific capacity.
     *
     *
     *             (1,1,1)
     *       +---------+
     *      /'  T3    /|T4
     *     +---------+ |
     *     | ' H _\  |T1
     *     |T0  / /  | |
     *     | +..\__/.|.+
     *   T5|,  T2    |/
     *     +---------+
     * (0,0,0)
     *
     * T(time) = T(0) * exp(-nu * time) + Tinf * (1 - exp(-nu * time))
     * nu = (6 * H) / (RHO * CP)
     * T(inf) = (T0 + T1 + T2 + T3 + T4 + T5) / 6
     */


    $ stardis -V 2 -M model.txt -m CUBE -e
    Temperature in medium 'CUBE' at t=inf = 325.093 K +/- 0.171141
    #failures: 0/10000

Expected T=325°K

    $ stardis -V 2 -M model.txt -p 0.5,0.5,0.5,inf -e
    warning: Probe is in fluid 'CUBE': computing fluid temperature, not using a specific position.
    Temperature at [0.5, 0.5, 0.5] at t=inf = 325.205 K +/- 0.170375
    #failures: 0/10000

Expected T=325°K

    $ stardis -V 3 -M bc.txt -M material.txt -p 0.5,0.5,0.5,inf -e
    output: Use 8 threads.
    output: Dummy solid created: (it is medium 0)
    output: Scaling factor is 1
    output: Trad is 300, Trad reference is 300
    output: Description 0: H boundary for fluid 'FLUX1': ref_temperature=300 emissivity=0 specular_fraction=0 hc=10 T=300 (using medium 0 as external medium)
    output: Description 1: H boundary for fluid 'FLUX2': ref_temperature=300 emissivity=0 specular_fraction=0 hc=10 T=310 (using medium 0 as external medium)
    output: Description 2: H boundary for fluid 'FLUX3': ref_temperature=300 emissivity=0 specular_fraction=0 hc=10 T=320 (using medium 0 as external medium)
    output: Description 3: H boundary for fluid 'FLUX4': ref_temperature=300 emissivity=0 specular_fraction=0 hc=10 T=330 (using medium 0 as external medium)
    output: Description 4: H boundary for fluid 'FLUX5': ref_temperature=300 emissivity=0 specular_fraction=0 hc=10 T=340 (using medium 0 as external medium)
    output: Description 5: H boundary for fluid 'FLUX6': ref_temperature=300 emissivity=0 specular_fraction=0 hc=10 T=350 (using medium 0 as external medium)
    output: Description 6: Fluid 'CUBE': rho=25 cp=2 Tinit=280 (it is medium 1)
    output: Probe is in solid 'CUBE'.
    warning: Probe is in fluid 'CUBE': computing fluid temperature, not using a specific position.
    output: Solving probe temperature: 100%
    Temperature at [0.5, 0.5, 0.5] at t=inf = 325.078 K +/- 0.171276
    #failures: 0/10000

Expected T=325°K

    $ stardis -V 2 -M model.txt -m CUBE,1
    311.181 0.251689 0 10000

Expected T=311.45°K
