
Test test_convection_non_uniform from stardis-solver project
============================================================

    /*
     * The scene is composed of an unit fluid cube whose temperature is
     * unknown. The convection coefficient with the surrounding solid is H
     * everywhere the temperature of the -/+X, -/+Y and -/+Z faces are fixed to T0
     * and T1, T2, T3, T4 and T5, respectively.  This test computes the temperature
     * of the fluid Tf at an observation time t. This temperature is equal to:
     *
     *    Tf(t) = Tf(0) * e^(-nu*t) + Tinf*(1-e^(-nu*t))
     *
     *    nu = (Sum_{i=0..5}(H*Si)) / (RHO*CP*V)
     *    Tinf = (Sum_{i=0..5}(H*Si*Ti) / (Sum_{i=0..5}(H*Si));
     *
     * with Si surface of the faces (i.e. one), V the volume of the cube (i.e.
     * one), RHO the volumic mass of the fluid and CP its calorific capacity.
     *
     *
     *             (1,1,1)
     *       +---------+
     *      /'  T3    /|T4
     *     +---------+ |
     *     | ' H _\  |T1
     *     |T0  / /  | |
     *     | +..\__/.|.+
     *   T5|,  T2    |/
     *     +---------+
     * (0,0,0)
     *
     * T(time) = T(0) * exp(-nu * time) + Tinf * (1 - exp(-nu * time))
     * nu = Sum(Hi * Si) / (RHO * CP * V)
     * T(inf) = Sum(Hi * Si * Ti) / Sum(Hi * Si)
     */

     
    $ stardis -V 2 -M model.txt -m CUBE -e
    Temperature in medium 'CUBE' at t=inf = 330.075 K +/- 0.0733378
    #failures: 0/10000

Expected T=330.09°K

    $ stardis -V 2 -M model.txt -p 0.5,0.5,0.5,INF
    warning: Probe is in fluid 'CUBE': computing fluid temperature, not using a specific position.
    330.105 0.0744976 0 10000

Expected T=330.09°K

    $ stardis -V 2 -M model.txt -m CUBE,0.005
    317.785 0.225265 0 10000

Expected T=317.61°K
