#!/bin/bash
# Copyright (C) 2018-2020 |Meso|Star>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Use 'source ...../etc/stardis.profile' before running this script
# This script can either be run without arg in the StarterPack directory
# or in any directory by providing the path to the StarterPAck directory
# as first and only arg
if [[ $# == 1 ]]; then
  export SP_ROOT=`realpath $1`
else
  export SP_ROOT=`realpath .`
fi
  
# functions that echo the command and run it
# expecting success
run_ok() {
  echo "$@"
  if ! eval $@
  then
    echo Command failed! >&2
    exit
  fi
}
# expecting failure
run_ko() {
  echo "$@"
  if eval $@
  then
    echo Command failed! >&2
    exit
  fi
}

function check()
{
  echo "Reference: $1"
  echo "Result: $2"
  count=$(echo $1 | wc -w)
  for ((i=1; i<=$count; i++))
  {
    idxmc=$((2 * $i - 1))
    idxstd=$((2 * $i))
    ref=$(echo $1 | cut -d ' ' -f $i)
    mc=$(echo $2 | cut -d ' ' -f $idxmc)
    std=$(echo $2 | cut -d ' ' -f $idxstd)

    local min=$(echo "scale=9; $mc - 3 * $std" | bc)
    local max=$(echo "scale=9; $mc + 3 * $std" | bc)
    local min_ok=$(echo "$min <= $ref" | bc)
    local max_ok=$(echo "$max >= $ref" | bc)
    if [ $min_ok -ne 1 ] || [ $max_ok -ne 1 ]
    then
      echo "Error: Result out of 3 sigma range: $mc VS [$min   $max]" >&2
      exit 1
    fi
  }
} 

# functions that echo the command, run it, and check result(s) against a reference
# expecting success
run_ok_check() {
  echo "$@"
  local expected=$1
  local cmd=$2
  result=$( $cmd )
  if [ $? -ne 0 ]
  then
    echo "Error: Command failed!" >&2
    exit 1
  fi
  re='^[0-9 .eEgG+-]+$'
  if ! [[ $result =~ $re ]] ; then
    echo "Error: result is not a number: $result" >&2
    echo "Error: don't use the option -e with stardis" >&2
    exit 1
  fi
  check "$expected" "$result"
  echo "Message: Result(s) in 3 sigma range"
}

function compat()
{
  mc1=$(echo $1 | cut -d ' ' -f 1)
  std1=$(echo $1 | cut -d ' ' -f 2)
  mc2=$(echo $2 | cut -d ' ' -f 1)
  std2=$(echo $2 | cut -d ' ' -f 2)
  echo "MC result #1: $mc1 +/- $std1"
  echo "MC result #2: $mc2 +/- $std2"

  local diff=$(echo "define abs(x) { if (x>=0) return(x); scale=9; { return(-x); } } ; abs($mc1 - $mc2)" | bc)
  local sigma=$(echo "scale=9; 2 * ($std1 + $std2)" | bc)

  local ok=$(echo "$sigma >= $diff" | bc)
  if [ $ok -ne 1 ]
  then
    echo "Error: Incompatible results  $mc1 +/- $std1 VS $mc2 +/- $std2" >&2
    exit 1
  fi
} 

# functions that echo the command, run it, and check result(s) against a reference
# expecting success
run_ok_compat() {
  echo "$@"
  local mc1=$1
  local mc2=$2
  result1=$( $mc1 )
  if [ $? -ne 0 ]
  then
    echo "Error: Command failed!" >&2
    exit 1
  fi
  re='^[0-9 .eEgG+-]+$'
  if ! [[ $result1 =~ $re ]] ; then
    echo "Error: result is not a number: $result1" >&2
    echo "Error: don't use the option -e with stardis" >&2
    exit 1
  fi
  result2=$( $mc2 )
  if [ $? -ne 0 ]
  then
    echo "Error: Command failed!" >&2
    exit 1
  fi
  re='^[0-9 .eEgG+-]+$'
  if ! [[ $result2 =~ $re ]] ; then
    echo "Error: result is not a number: $result2" >&2
    echo "Error: don't use the option -e with stardis" >&2
    exit 1
  fi
  compat "$result1" "$result2"
  echo "Message: Results are compatible"
}

#
# COMPUTE
#

run_ok "cd ${SP_ROOT}/Compute/boundary"

run_ko "stardis -V 2 -M model.txt -P 0.5,0.2,0.8 -e"
echo "Failure expected"
run_ok_check "308.33" "stardis -M model.txt -P 1,0.6,0.3"
run_ok_check "308.33" "stardis -M model.txt -s right_bc.stl"
run_ok_check "304.17" "stardis -M model.txt -p 0.5,0.2,0.8"
run_ok_check "304.17" "stardis -M model.txt -P 0.5,0.2,0.98"
run_ok_check "308.33" "stardis -M model_prog.txt -P 1,0.6,0.3"
run_ok_check "308.33" "stardis -M model_prog.txt -s right_bc.stl"
run_ok_check "304.17" "stardis -M model_prog.txt -p 0.5,0.2,0.8"
run_ok_check "304.17" "stardis -M model_prog.txt -P 0.5,0.2,0.98"

run_ok "cd ${SP_ROOT}/Compute/boundary_flux"

run_ko "stardis -V 2 -M model.txt -P 0.5,0.2,0.8,inf"
echo "Failure expected"
run_ko "stardis -V 2 -M model_prog.txt -P 0.5,0.2,0.8,inf"
echo "Failure expected"
run_ok_check "295.54" "stardis -M model.txt -P 1,0.6,0.3,inf"
run_ok_check "295.54" "stardis -M model.txt -s right_bc.stl"
run_ok_check "295.54 -2.23 -27.32 0 -29.5538" "stardis -M model.txt -F right_bc.stl,inf -n 200000"
run_ok_check "295.54 -2.23 -27.32 0 -29.5538" "stardis -M model.txt -F right_bc.stl,inf -n 200000"
run_ok_check "147.77" "stardis -M model.txt -p 0.5,0.2,0.8,inf -n 100000"
run_ok_check "0" "stardis -M model.txt -s left_bc.stl,inf"
run_ok_check "0" "stardis -M model.txt -P 0,0.3,0.4"
run_ok_check "295.54" "stardis -M model_prog.txt -P 1,0.6,0.3,inf"
run_ok_check "295.54" "stardis -M model_prog.txt -s right_bc.stl"
run_ok_check "295.54 -2.23 -27.32 0 -29.5538" "stardis -M model_prog.txt -F right_bc.stl,inf -n 200000"
run_ok_check "295.54 -2.23 -27.32 0 -29.5538" "stardis -M model_prog.txt -F right_bc.stl,inf -n 200000"
run_ok_check "147.77" "stardis -M model_prog.txt -p 0.5,0.2,0.8,inf -n 100000"
run_ok_check "0" "stardis -M model_prog.txt -s left_bc.stl,inf"
run_ok_check "0" "stardis -M model_prog.txt -P 0,0.3,0.4"

run_ok "cd ${SP_ROOT}/Compute/conducto_radiative"

run_ok_check "305" "stardis -M model.txt -p 0,0,0,inf"
run_ok_check "307.5" "stardis -M model.txt -p 0.5,0.9,-0.9"
run_ok_check "300" "stardis -M model.txt -p 1.25,0,0,INF"
run_ok_check "305" "stardis -M model_prog.txt -p 0,0,0,inf"
run_ok_check "307.5" "stardis -M model_prog.txt -p 0.5,0.9,-0.9"
run_ok_check "300" "stardis -M model_prog.txt -p 1.25,0,0,INF"

run_ok "cd ${SP_ROOT}/Compute/convection"

run_ok_check "325" "stardis -M model.txt -m CUBE"
run_ok_check "325" "stardis -M model.txt -p 0.5,0.5,0.5,INF"
run_ok_check "325" "stardis -M model.txt -p 0.5,0.5,0.5,INF"
run_ok_check "311.45" "stardis -M model.txt -m CUBE,1"
run_ok_check "325" "stardis -M model_prog.txt -m CUBE"
run_ok_check "325" "stardis -M model_prog.txt -p 0.5,0.5,0.5,INF"
run_ok_check "325" "stardis -M model_prog.txt -p 0.5,0.5,0.5,INF"
run_ok_check "311.45" "stardis -M model_prog.txt -m CUBE,1"

run_ok "cd ${SP_ROOT}/Compute/convection_non_uniform"

run_ok_check "330.09" "stardis -M model.txt -m CUBE"
run_ok_check "330.09" "stardis -M model.txt -p 0.5,0.5,0.5,INF"
run_ok_check "317.61" "stardis -M model.txt -m CUBE,0.005"
run_ok_check "330.09" "stardis -M model_prog.txt -m CUBE"
run_ok_check "330.09" "stardis -M model_prog.txt -p 0.5,0.5,0.5,INF"
run_ok_check "317.61" "stardis -M model_prog.txt -m CUBE,0.005"

run_ok "cd ${SP_ROOT}/Compute/flux"

run_ok_check "370" "stardis -M model.txt -p 0.5,0.5,0.5,inf -n 40000"
run_ok_check "410" "stardis -M model.txt -p 0.1,0.75,0.85 -n 40000"
run_ok_check "395" "stardis -M model.txt -p 0.25,0.25,0.25 -n 40000"
run_ok_check "395" "stardis -M model.txt -P 0.25,0,0.25 -n 40000"
run_ok_check "370" "stardis -M model_prog.txt -p 0.5,0.5,0.5,inf -n 40000"
run_ok_check "410" "stardis -M model_prog.txt -p 0.1,0.75,0.85 -n 40000"
run_ok_check "395" "stardis -M model_prog.txt -p 0.25,0.25,0.25 -n 40000"
run_ok_check "395" "stardis -M model_prog.txt -P 0.25,0,0.25 -n 40000"

run_ok "cd ${SP_ROOT}/Compute/green_1"

run_ok "stardis -V 2 -M model.txt -p 0,0,0 -n 1000 -g > probe_green.txt"
run_ok "stardis -V 2 -M model.txt -p 0,0,0 -n 100000 -G probe.green"
run_ok "sgreen -V 2 -g probe.green -s green.html"
run_ok "sgreen -V 2 -g probe.green -a settings.txt"
run_ok "sgreen -e -g probe.green -a settings.txt"
run_ok "stardis -V 2 -M model_100_360.txt -p 0,0,0 -n 100000"

run_ok "cd ${SP_ROOT}/Compute/green_2"

run_ok "stardis -V 2 -M model.txt -p 0,0,0 -n 1000 -g > probe_green.txt"
run_ok "stardis -V 2 -M model.txt -p 0,0,0 -n 100000 -G probe.green"
run_ok "sgreen -V 2 -g probe.green -s green.html"
run_ok "sgreen -V 2 -g probe.green -a settings.txt"
run_ok "sgreen -e -g probe.green -a settings.txt"
run_ok "stardis -V 2 -M model_100_360.txt -p 0,0,0 -n 100000"

run_ok "cd ${SP_ROOT}/Compute/green_3"

run_ok "stardis -V 2 -M model.txt -p 0,0,0 -n 20000 -G probe.green"
run_ok "time sgreen -g probe.green -a settings.txt > res_n.txt"
run_ok "time sgreen -g probe.green -a settings.txt -t 1 > res_1.txt"
run_ok "diff res_n.txt res_1.txt"

run_ok "cd ${SP_ROOT}/Compute/media_connections"

for t in 0 10 100 1000
do
  run_ok "diff <(stardis -M model.txt -n 1000 -p 0,0,0,$t) <(stardis -M model_prog.txt -n 1000 -p 0,0,0,$t)"
  run_ok "diff <(stardis -M model.txt -n 1000 -p 0.5,0.9,-0.9,$t) <(stardis -M model_prog.txt -n 1000 -p 0.5,0.9,-0.9,$t)"
  run_ok "diff <(stardis -M model.txt -n 1000 -p 0.205,0.505,-0.395,$t) <(stardis -M model_prog.txt -n 1000 -p 0.205,0.505,-0.395,$t)"
done

run_ok "cd ${SP_ROOT}/Compute/scale_factor"

run_ok_compat "stardis -M model_m_prog.txt -p +0.9,0,0,inf -n 100000" "stardis -M model_mm.txt -p +900,0,0,inf -n 100000"
run_ok_compat "stardis -M model_m_prog.txt -p -0.9,0,0,inf -n 400000" "stardis -M model_mm.txt -p -900,0,0,inf -n 400000"

run_ok "cd ${SP_ROOT}/Compute/volumic_power"

run_ok_check "329.375" "stardis -M model.txt -p 0.25,0.25,0.25,inf"
run_ok_check "332.5" "stardis -M model.txt -p 0.5,0.5,0.5"
run_ok_check "329.375" "stardis -M model_prog.txt -p 0.25,0.25,0.25,inf"
run_ok_check "332.5" "stardis -M model_prog.txt -p 0.5,0.5,0.5"

#
# DUMP
#

run_ok "cd ${SP_ROOT}/Dump/compute_region"

# Geometry only
run_ok "stardis -V 2 -M material.txt -M bc.txt -d > no_region.vtk"
# Geometry and surface compute region
run_ok "stardis -V 2 -M material.txt -M bc.txt -s surface.stl,INF -d > surface_region.vtk"
# Geometry and invalid surface compute region
run_ok "stardis -V 2 -M material.txt -M bc.txt -s surface_err.stl,INF -d > surface_region_err.vtk"
# Geometry and volume compute region
run_ok "stardis -V 2 -M material.txt -M bc.txt -m AL,INF -d > volume_region.vtk"

run_ok "cd ${SP_ROOT}/Dump/errors"

# Valid geometry
run_ok "stardis -V 2 -M valid_material.txt -M no_error_bc.txt -d > no_error.vtk"
# Invalid enclosure
run_ok "stardis -V 2 -M invalid_material.txt -M no_error_for_invalid_bc.txt -d > invalid_enclosure.vtk"
# Invalid surface compute region
run_ok "stardis -V 2 -M valid_material.txt -M no_error_bc.txt -s surface_err.stl -d > surface_region_err.vtk"
# Merge conflicts
run_ok "stardis -V 2 -M merge_conflict.txt -d > merge_conflict.vtk"
# Property conflicts (21 different flavours)
run_ok "stardis -V 2 -M valid_material.txt -M hf_2def_bc.txt -d > hf_2def_error.vtk"
run_ok "stardis -V 2 -M material_undef_center.txt -M hf_2undef_bc.txt -d > hf_2undef_error.vtk"
run_ok "stardis -V 2 -M valid_material.txt -M hf_solid_bc.txt -d > hf_solid_error.vtk"
run_ok "stardis -V 2 -M valid_material.txt -M hs_2def_bc.txt -d > hs_2def_error.vtk"
run_ok "stardis -V 2 -M material_undef_center.txt -M hs_2undef_bc.txt -d > hs_2undef_error.vtk"
run_ok "stardis -V 2 -M valid_material.txt -M hs_fluid_bc.txt -d > hs_fluid_error.vtk"
run_ok "stardis -V 2 -M valid_material.txt -M ts_2def_bc.txt -d > ts_2def_error.vtk"
run_ok "stardis -V 2 -M material_undef_center.txt -M ts_2undef_bc.txt -d > ts_2undef_error.vtk"
run_ok "stardis -V 2 -M valid_material.txt -M ts_fluid_bc.txt -d > ts_fluid_error.vtk"
run_ok "stardis -V 2 -M valid_material.txt -M fs_2def_bc.txt -d > fs_2def_error.vtk"
run_ok "stardis -V 2 -M material_undef_center.txt -M fs_2undef_bc.txt -d > fs_2undef_error.vtk"
run_ok "stardis -V 2 -M valid_material.txt -M fs_fluid_bc.txt -d > fs_fluid_error.vtk"
run_ok "stardis -V 2 -M 2solids.txt -M sf_2s_bc.txt -d > sf_2s_error.vtk"
run_ok "stardis -V 2 -M 2fluids.txt -M sf_2f_bc.txt -d > sf_2f_error.vtk"
run_ok "stardis -V 2 -M valid_material.txt -M sf_bound_bc.txt -d > sf_bound_error.vtk"
run_ok "stardis -V 2 -M material_undef_center.txt -M sf_2undef_bc.txt -d > sf_2undef_error.vtk"
run_ok "stardis -V 2 -M 2fluids.txt -M undef_2f_bc.txt -d > undef_2f_error.vtk"
run_ok "stardis -V 2 -M valid_material.txt -M undef_sf_bc.txt -d > undef_sf_error.vtk"
run_ok "stardis -V 2 -M valid_material.txt -M undef_uf_bc.txt -d > undef_uf_error.vtk"
run_ok "stardis -V 2 -M valid_material.txt -M undef_us_bc.txt -d > undef_us_error.vtk"
run_ok "stardis -V 2 -M material_undef_center.txt -M ts_undef_center_bc.txt -s cube_2_ext.stl -d > surf_region_error.vtk"
	  
run_ok "cd ${SP_ROOT}/Dump/heatsink"

run_ok "stardis -V 2 -M material.txt -M bc.txt -d > no_conflict.vtk"
run_ok "stardis -V 2 -M material_merge_conflicts.txt -M bc.txt -d > merge_conflicts.vtk"
run_ok "stardis -V 2 -M material_property_conflicts.txt -M bc.txt -d > property_conflicts.vtk"

run_ok "cd ${SP_ROOT}/Dump/hole"

run_ok "stardis -V 2 -M model.txt -d > no_hole.vtk"
run_ok "stardis -V 2 -M model_hole.txt -d > hole.vtk"

run_ok "cd ${SP_ROOT}/Dump/paths"

run_ok "stardis -V 2 -M module.txt -m PUCE,INF -n 10 -D all,paths_ -e"

#
# Rendering
#

run_ok "cd ${SP_ROOT}/Rendering/cube"

run_ok "stardis -V 2 -M material.txt -M bc.txt -R pos=2,0.5,0.5:tgt=0.5,0.5,0.5:file=image_1.ht"
run_ok "stardis -V 2 -M material.txt -M bc.txt -R pos=2,1.3,1.3:tgt=0.5,0.5,0.5:file=image_2.ht"
run_ok "stardis -V 2 -M material.txt -M bc.txt -R pos=-1,1.3,1.3:tgt=0.5,0.5,0.5:fmt=vtk > image3.vtk"

run_ok "cd ${SP_ROOT}/Rendering/heatsink"

run_ok "stardis -V 2 -M model.txt -d > model.vtk"
run_ok "stardis -V 2 -M model.txt -D error,path_1_ -R img=60x40:pos=100,-40,40:tgt=-60,30,-30:up=0,0,-1:fov=36:spp=1:fmt=vtk:file=image_1.vtk"
run_ok "ls path_1_*;:"
run_ok "stardis -V 2 -M model.txt -D error,path_2_ -R img=60x40:pos=60,20,30:tgt=-50,20,-25:up=0,0,-1:fov=36:spp=1 > image_2.ht"
run_ok "ls path_2_*;:"
