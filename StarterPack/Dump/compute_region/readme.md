Dump without compute region
===========================

    $ stardis -V 2 -M material.txt -M bc.txt -d > no_region.vtk
      
Field **Compute_region** is not available.

Dump with surface compute region
================================
The compute region involves 2 triangles, one on its front side only, the other
on its two sides.

    $ stardis -V 2 -M material.txt -M bc.txt -s surface.stl,INF -d > surface_region.vtk


Field **Compute_region** is available. Possible values are:
- 0 : The triangle is not part of the compute region.
- 1 : The front side of the triangle is part of the compute region.
- 2 : The back side of the triangle is part of the compute region.
- 3 : Both the front and back sides of the triangle are part of the compute region.
- 4294967295 : The triangle is part of the compute region, but is not
part of the geometry as defined through the **-M** command line option;
this is an error.

Dump with wrong surface compute region
======================================
The compute region involves 3 triangles. One of them is not member of the geometry,
thus triggering an error.


    $ stardis -V 2 -M material.txt -M bc.txt -s surface_err.stl,INF -d > surface_region_err.vtk
    warning: Invalid compute region defined by file 'surface_err.stl'.
    warning: The file contains 1 triangles not in the model.
      

Field **Compute_region** is available. Same possible values as above.

Field **Property_conflict** is available. The triangle not member of the geometry
has error value 24 (triangle in compute region not member of the geometry), others
show no error (value 0).

Dump with volume compute region
===============================

    $ stardis -V 2 -M material.txt -M bc.txt -m AL,INF -d > volume_region.vtk

Field **Compute_region** is available. Same possible values as above.
