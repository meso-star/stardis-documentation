
  FLUID     LF         1    1          300    UNKNOWN         FRONT left_fluid.stl
  SOLID    SOL   0.1   1    1   0.1    300    UNKNOWN   100   FRONT solid.stl
  FLUID     RF         1    1          300    UNKNOWN         FRONT right_fluid.stl

  H_BOUNDARY_FOR_FLUID     LMOST  300   1   1   0   300   leftmost_bc.stl
  H_BOUNDARY_FOR_FLUID      LEFT  300   0   0   0   0     left_bc.stl
  SOLID_FLUID_CONNECTION   CLEFT  300   1   0   5         center_left_bc.stl
  F_BOUNDARY_FOR_SOLID    CENTER  0                       center_bc.stl
  SOLID_FLUID_CONNECTION  CRIGHT  300   1   0   5         center_right_bc.stl
  H_BOUNDARY_FOR_FLUID     RIGHT  300   0   0   0   0     right_bc.stl
  H_BOUNDARY_FOR_FLUID     RMOST  300   1   1   0   360   rightmost_bc.stl
  