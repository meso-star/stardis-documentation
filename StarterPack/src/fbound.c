static const char copyright_notice[] =
"Copyright (C) 2021-2022 |Meso|Star> (contact@meso-star.com).";

static const char license_short[] =
"Licensed under GPLv3, see <http://www.gnu.org/licenses/>.";

static const char license_text[] =
"This program is free software: you can redistribute it and/or modify\n\
it under the terms of the GNU General Public License as published by\n\
the Free Software Foundation, either version 3 of the License, or\n\
(at your option) any later version.\n\
This program is distributed in the hope that it will be useful,\n\
but WITHOUT ANY WARRANTY; without even the implied warranty of\n\
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the\n\
GNU General Public License for more details.\n\
You should have received a copy of the GNU General Public License\n\
along with this program. If not, see <http://www.gnu.org/licenses/>.";

/* Build:
 * gcc -shared -g -I ${stardis_path}/include -o libfbound.so -fPIC fbound.c
 */

#include <stardis/stardis-prog-properties.h>

#include <float.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#define CHK_ARG(Name) \
  if(argc <= idx) {\
    fprintf(stderr,\
       "Flux boundary '%s': invalid data (missing token '" Name "')\n",\
       bound->name);\
    goto error;\
  } else arg = argv[idx++]

struct my_fbound {
  char name[128];
  double flux;
};

void
stardis_release_data
  (void* my_fbound)
{
  assert(my_fbound);
  free(my_fbound);
}

void*
stardis_create_data
  (const struct stardis_description_create_context* ctx,
   void* data,
   size_t argc,
   char* argv[])
{
  struct my_fbound* bound = NULL;
  char *arg, *end;
  size_t idx = 0;

  (void)data;

  if(!argv || ! ctx) goto error;
  bound = malloc(sizeof(*bound));
  if(!bound) goto error;

  strncpy(bound->name, ctx->name, 127);

  /* argv[0] = flux */
  CHK_ARG("flux");
  bound->flux = strtod(arg, &end);
  if(bound->flux == DBL_MAX || arg == end) {
    fprintf(stderr, "Flux boundary '%s': invalid flux: %s\n", bound->name, arg);
    goto error;
  }
  if(argc > idx) {
    fprintf(stderr, "Flux boundary '%s': trailing arguments:\n", bound->name);
    for( ; idx < argc; idx++) fprintf(stderr, "%s\n", argv[idx]);
    goto error;
  }

end:
  return bound;
error:
  if(bound) stardis_release_data(bound);
  bound = NULL;
  goto end;
}

const char*
get_copyright_notice
  (void* data)
{
  (void)data;
  return copyright_notice;
}

const char*
get_license_short
  (void* data)
{
  (void)data;
  return license_short;
}

const char*
get_license_text
  (void* data)
{
  (void)data;
  return license_text;
}

double
stardis_boundary_flux
  (const struct stardis_interface_fragment* frag,
   void* data)
{
  struct my_fbound* bound = data;
  (void)frag;
  return bound->flux;
}
