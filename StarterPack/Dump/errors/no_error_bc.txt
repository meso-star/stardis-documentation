
#      +----1----+----2----+----1----+
#      |         |         |         |
#      1    AL   3   AIR   3   AL    1
#      |         |         |         |
#      +----1----+----2----+----1----+
#
# 1: T_BOUNDARY_FOR_SOLID
# 2: H_BOUNDARY_FOR_FLUID
# 3: SOLID_FLUID_CONNECTION

  T_BOUNDARY_FOR_SOLID     CL1   300  cube_1_ext.stl cube_3_ext.stl
  H_BOUNDARY_FOR_FLUID     CL2   300  1  0  5  300   cube_2_ext.stl
  SOLID_FLUID_CONNECTION   CL3   300  1  0  5   cube_1_2.stl cube_2_3.stl
  
