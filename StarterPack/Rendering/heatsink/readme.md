Infrared rendering of an heatsink
=================================

    /* The model is a simplified heatsink.
     * A small region in the model is a volumic power source.
     * The image is tiny with a single sample per pixel, and the h coefficient
     * is set to a huge value, all of these to ensure that rendering time is in
     * minutes, not hours or days.
     *
     *
     *          +---------------------+    
     *         /  +------+           /|
     *        /  / Power/           /||
     *       /  +------+           /|||
     *      /                     /|||
     *     +---------------------+|||
     *     |                     |||
     *     |                     ||
     *     |_____________________| 
     *
     */

      $ stardis -V 2 -M model.txt -d > model.vtk

      $ stardis -V 2 -M model.txt -a 280 -D error,path_1_ -R img=60x40:pos=100,-40,40:tgt=-60,30,-30:up=0,0,-1:fov=36:spp=1:fmt=vtk:file=image_1.vtk

      $ ls path_1_*
      ls: cannot access 'path_1_*': No such file or directory


      $ stardis -V 2 -M model.txt -a 280 -D error,path_2_ -R img=60x40:pos=60,20,30:tgt=-50,20,-25:up=0,0,-1:fov=36:spp=1 > image_2.ht

      $ ls path_2_*
      ls: cannot access 'path_2_*': No such file or directory

