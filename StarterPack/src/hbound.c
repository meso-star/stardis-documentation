static const char copyright_notice[] =
"Copyright (C) 2021-2022 |Meso|Star> (contact@meso-star.com).";

static const char license_short[] =
"Licensed under GPLv3, see <http://www.gnu.org/licenses/>.";

static const char license_text[] =
"This program is free software: you can redistribute it and/or modify\n\
it under the terms of the GNU General Public License as published by\n\
the Free Software Foundation, either version 3 of the License, or\n\
(at your option) any later version.\n\
This program is distributed in the hope that it will be useful,\n\
but WITHOUT ANY WARRANTY; without even the implied warranty of\n\
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the\n\
GNU General Public License for more details.\n\
You should have received a copy of the GNU General Public License\n\
along with this program. If not, see <http://www.gnu.org/licenses/>.";

/* Build:
 * gcc -shared -g -I ${stardis_path}/include -o libhbound.so -fPIC hbound.c
 */

#include <stardis/stardis-prog-properties.h>

#include <float.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#define CHK_ARG(Name) \
  if(argc <= idx) {\
    if(bound->verbosity >= STARDIS_VERBOSE_ERROR) {\
      fprintf(stderr,\
         "H boundary '%s': invalid data (missing token '" Name "')\n",\
         bound->name);\
    }\
    goto error;\
  } else arg = argv[idx++]

struct my_hbound {
  char name[128];
  enum stardis_verbosity_levels verbosity;
  double emissivity, alpha, hc, temp, ref_temp;
};

struct my_lib_data {
  enum stardis_verbosity_levels verbosity;
};

void*
stardis_create_library_data
  (const struct stardis_program_context* ctx,
   size_t argc,
   char** argv)
{
  struct my_lib_data* data;
  data = malloc(sizeof(*data));
  if(!data) return NULL;
  data->verbosity = ctx->verbosity_level;
  return data;
}

enum stardis_return_status
stardis_finalize_library_data
  (void* data_)
{
  struct my_lib_data* data = data_;
  if(data->verbosity >= STARDIS_VERBOSE_INFO)
    printf("libhound: finalizing library data\n");
  return STARDIS_SUCCESS;
}

void
stardis_release_library_data
  (void* data_)
{
  struct my_lib_data* data = data_;
  if(data->verbosity >= STARDIS_VERBOSE_INFO)
    printf("libhound: releasing library data\n");
  free(data);
}

void
stardis_release_data
  (void* data)
{
  struct my_hbound* bound = data;
  if(bound->verbosity >= STARDIS_VERBOSE_INFO)
    printf("libhound: releasing data for %s\n", bound->name);
  free(bound);
}

void*
stardis_create_data
  (const struct stardis_description_create_context* ctx,
   void* data_,
   size_t argc,
   char* argv[])
{
  struct my_hbound* bound = NULL;
  struct my_lib_data* data = data_;
  char *arg, *end;
  size_t idx = 0;

  if(!data || !argv) goto error;

  bound = malloc(sizeof(*bound));
  if(!bound) goto error;

  strncpy(bound->name, ctx->name, 127);
  bound->verbosity = data->verbosity;
  if(data->verbosity >= STARDIS_VERBOSE_INFO)
    printf("libhound: creating data for %s\n", bound->name);

  /* argv[0] = reference temperature */
  CHK_ARG("reference temperature");
  bound->ref_temp = strtod(arg, &end);
  if(bound->ref_temp < 0 || arg == end) {
    if(bound->verbosity >= STARDIS_VERBOSE_ERROR)
      fprintf(stderr, "H boundary '%s': Invalid Reference temperature: %s\n",
        bound->name, arg);
    goto error;
  }
  /* argv[1] = emissivity */
  CHK_ARG("emissivity");
  bound->emissivity = strtod(arg, &end);
  if(bound->emissivity < 0 || bound->emissivity > 1 || arg == end) {
    if(bound->verbosity >= STARDIS_VERBOSE_ERROR)
      fprintf(stderr, "H boundary '%s': Invalid Emissivity: %s\n",
        bound->name, arg);
    goto error;
  }
  /* argv[2] = specular fraction */
  CHK_ARG("specular fraction");
  bound->alpha = strtod(arg, &end);
  if(bound->alpha < 0 || bound->alpha > 1 || arg == end) {
    if(bound->verbosity >= STARDIS_VERBOSE_ERROR)
      fprintf(stderr, "H boundary '%s': Invalid Specular fraction: %s\n",
        bound->name, arg);
    goto error;
  }
  /* argv[3] = convection coefficient */
  CHK_ARG("convection coefficient");
  bound->hc = strtod(arg, &end);
  if(bound->hc < 0 || arg == end) {
    if(bound->verbosity >= STARDIS_VERBOSE_ERROR)
      fprintf(stderr, "H boundary '%s': Invalid Convection coefficient: %s\n",
        bound->name, arg);
    goto error;
  }
  /* argv[4] = environment temperature */
  CHK_ARG("environment temperature");
  bound->temp = strtod(arg, &end);
  if(bound->temp < 0 || arg == end) {
    if(bound->verbosity >= STARDIS_VERBOSE_ERROR)
      fprintf(stderr, "H boundary '%s': Invalid Tenv: %s\n", bound->name, arg);
    goto error;
  }
  if(argc > idx) {
    fprintf(stderr, "H boundary '%s': trailing arguments:\n", bound->name);
    for( ; idx < argc; idx++) fprintf(stderr, "%s\n", argv[idx]);
    goto error;
  }

end:
  return bound;
error:
  if(bound) stardis_release_data(bound);
  bound = NULL;
  goto end;
}

const char*
get_copyright_notice
  (void* data)
{
  (void)data;
  return copyright_notice;
}

const char*
get_license_short
  (void* data)
{
  (void)data;
  return license_short;
}

const char*
get_license_text
  (void* data)
{
  (void)data;
  return license_text;
}

double
stardis_emissivity
  (const struct stardis_interface_fragment* frag,
   void* data)
{
  struct my_hbound* bound = data;
  (void)frag;
  return bound->emissivity;
}

double
stardis_specular_fraction
  (const struct stardis_interface_fragment* frag,
   void* data)
{
  struct my_hbound* bound = data;
  (void)frag;
  return bound->alpha;
}

double
stardis_convection_coefficient
  (const struct stardis_interface_fragment* frag,
   void* data)
{
  struct my_hbound* bound = data;
  (void)frag;
  return bound->hc;
}

double
stardis_max_convection_coefficient
  (void* data)
{
  struct my_hbound* bound = data;
  return bound->hc;
}

double
stardis_medium_temperature
  (const struct stardis_vertex* vrtx,
   void* data)
{
  struct my_hbound* bound = data;
  (void)vrtx;
  return bound->temp;
}

double
stardis_boundary_temperature
  (const struct stardis_interface_fragment* frag,
   void* data)
{
  struct my_hbound* bound = data;
  (void)frag;
  return bound->temp;
}

double
stardis_reference_temperature
  (const struct stardis_interface_fragment* frag,
   void* data)
{
  struct my_hbound* bound = data;
  (void)frag;
  return bound->ref_temp;
}

double*
stardis_t_range
  (void* data,
   double range[2])
{
  struct my_hbound* bound = data;
  range[0] = fmin(range[0], fmin(bound->temp, bound->ref_temp));
  range[1] = fmax(range[1], fmax(bound->temp, bound->ref_temp));
  return range;
}
