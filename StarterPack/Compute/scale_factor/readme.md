Test of scaling factors
=======================

    /* The scene involves most of the radiative transfer modes.
     * It is described twice: once in m and once in mm.
     * The test compares results from the 2 descriptions.
     *
     *                               (1, 1, 1)
     *                 +------+----------+------+ (1.5,1,1)
     *                /'     /'         /'     /|
     *               +------+----------+------+ |
     *               | '    | '        | '    | | 310K
     *        <====> | '    | ' 10W/m3 | '    | |
     *        10W/m2 | '    | '        | '    | |
     *               | +....| +........|.+....|.+
     *               |/     |/         |/     |/
     *  (-1.5,-1,-1) +------+----------+------+
     *                  (-1,-1,-1)
     *
     */

The 2 following results are expected to be compatible:

    $ stardis -M model_m_prog.txt -p +0.9,0,0,inf -n 100000
    346.237 0.454685 0 100000

    $ stardis -M model_mm.txt -p +900,0,0,inf -n 100000
    347.232 0.467012 0 100000

The 2 following results are expected to be compatible:

    $ stardis -M model_m_prog.txt -p -0.9,0,0,inf -n 400000
    698.096 0.566396 0 10000

    $ stardis -M model_mm.txt -p -900,0,0,inf -n 400000
    698.691 0.565552 0 10000
