#!/bin/sh

check()
{
  echo "Reference: $1"
  echo "Result: $2"
  count=$(echo "$1" | wc -w)
  i=1
  while [ $i -le "$count" ]; do
    idxmc=$((2 * i - 1))
    idxstd=$((2 * i))
    ref=$(echo "$1" | cut -d ' ' -f $i)
    mc=$(echo "$2" | cut -d ' ' -f $idxmc)
    std=$(echo "$2" | cut -d ' ' -f $idxstd)

    min=$(echo "scale=9; $mc - 3 * $std" | bc)
    max=$(echo "scale=9; $mc + 3 * $std" | bc)
    min_ok=$(echo "$min <= $ref" | bc)
    max_ok=$(echo "$max >= $ref" | bc)
    if [ "$min_ok" -ne 1 ] || [ "$max_ok" -ne 1 ]
    then
      echo "Error: Result out of 3 sigma range: $mc VS [$min   $max]" >&2
      exit 1
    fi
    i=$((i+1))
  done
  # check that no sample failed (last part of result: #failed #success)
  idxfailed=$((2 * i - 1))
  failed=$(echo "$2" | cut -d ' ' -f $idxfailed)
  if [ "$failed" -ne 0 ]
  then
    echo "Error: $failed sample(s) failed" >&2
    exit 1
  fi
} 

# run a command and checks result(s) against a reference
# check that 0 sample failed
# success expected
expected="$1"
cmd="$2"
if ! result=$( $cmd )
then
  echo "Error: Command failed ($cmd)!" >&2
  exit 1
fi
re='^[0-9 .eEgG+-]+$'
if ! echo "$result" | grep -Eq "$re" ; then
  echo "Error: result is not a number: $result" >&2
  echo "Error: don't use the option -e with stardis" >&2
  exit 1
fi
check "$expected" "$result"
echo "Message: Result(s) in 3 sigma range"
