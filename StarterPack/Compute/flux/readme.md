Test test_sdis_flux from stardis-solver project
===============================================

    /*
     * The scene is composed of a solid cube whose temperature is unknown.
     * The temperature is fixed at T0 on the +X face. The Flux of the -X face is
     * fixed to PHI. The flux on the other faces is null (i.e. adiabatic). This
     * test computes the temperature of a probe position pos into the solid and
     * check that it is equal to:
     *
     *    T(x,y,z) = T0 + (A - x) * PHI/LAMBDA = 320 + (1 - x) * 10 / 0.1
     *
     * with LAMBDA the conductivity of the solid and A=1 the size of cube.
     *
     *
     *       ///// (1,1,1)
     *       +-------+
     *      /'      /|
     *     +-------+ T0
     *   PHI +.....|.+
     *     |,      |/
     *     +-------+
     * (0,0,0) /////
     */


    $ stardis -V 3 -M model.txt -p 0.5,0.5,0.5,inf
    output: Use 8 threads.
    output: Dummy fluid created: (it is medium 1)
    output: Scaling factor is 1
    output: Trad is 300, Trad reference is 300
    output: Auto delta for solid 'CUBE' set to 0.0277778
    output: Description 0: Solid 'CUBE': lambda=0.1 rho=25 cp=2 delta=0.0277778 Tinit=300 (it is medium 0)
    output: Description 1: F boundary for SOLID 'FLUX': flux=10 (using medium 1 as external medium)
    output: Description 2: F boundary for SOLID 'ADIA': flux=0 (using medium 1 as external medium)
    output: Description 3: T boundary for solid' TEMP': T=320 (using medium 1 as external medium)
    output: Probe is in solid 'CUBE'.
    output: Probe is 18 delta from closest boundary.
    output: Solving probe temperature: 100%
    370.089 0.844627 0 10000
    Expected T=370

Expected T=370°K

    $ stardis -V 2 -M model.txt -p 0.1,0.75,0.85 -e
    Temperature at [0.1, 0.75, 0.85] at t=inf = 410.715 K +/- 0.98852
    #failures: 0/10000

Expected T=410°K

    $ stardis -V 2 -M model.txt -p 0.25,0.25,0.25 -e
    Temperature at [0.25, 0.25, 0.25 ] at t=inf = 395.078 K +/- 0.950186
    #failures: 0/10000

Expected T=395°K

    $ stardis -V 2 -M model.txt -P 0.25,0,0.25 -e
    Temperature at [0.25, 0.25, 0.25 ] at t=inf = 396.231 K +/- 0.96616
    #failures: 0/10000

Expected T=395°K
