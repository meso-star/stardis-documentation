Dump valid geometry
===================

The field **Property_conflict** is not present in resut file and the field
**Enclosures** does not report any enclosure with error.

    $ stardis -V 2 -M valid_material.txt -M no_error_bc.txt -d > no_error.vtk
      

Dump invalid enclosure
======================

The cube #2 contains 2 different media, thus the corresponding enclosure is
not suitable for stardis use and the field **Enclosures** report an error
for the corresponding enclosure.

Possible values can be :
* 0 for triangles not member of this enclosure,
* 1 for triangles member of this enclosure that is error free,
* 3, 5 or 7 for triangles member of this enclosure that has error(s)
(resp. encloses multiple media, encloses undefined triangles, or both).



      $ stardis -V 2 -M invalid_material.txt -M no_error_for_invalid_bc.txt -d > invalid_enclosure.vtk
      warning: Found 1 invalid enclosure(s).

Dump invalid surface region
===========================

The compute region involves 3 triangles. One of them is not member of the geometry,
thus triggering an error. As a consequence, the field **Compute_region** is present
in resut files. Also the faulty triangle has no property at all and forces the field
**Property_conflict** presence where it has the error value 24, whereas other
triangles have error value 0 (no error). 


Possible values are for field **Compute_region**:
- 0 : The triangle is not part of the compute region.
- 1 : The front side of the triangle is part of the compute region.
- 2 : The back side of the triangle is part of the compute region.
- 3 : Both the front and back sides of the triangle are part of the compute region.
- 4294967295 : The triangle is part of the compute region, but is not
part of the geometry as defined through the **-M** command line option;
this is an error.

      $ stardis -V 2 -M valid_material.txt -M no_error_bc.txt -s surface_err.stl -d > surface_region_err.vtk
      warning: Invalid compute region defined by file 'surface_err.stl'.
      warning: The file contains 1 triangles not in the model.


Dump geometries with merge errors
=================================

Some triangle sides in this example are first described as enclosing medium 'AL'
and later described as enclosing medium 'al'. As a consequence, the field
**Merge_conflict** is present in the resut file and the error code for each of
the involved triangles is set (value is **1**). The value associated to triangles
with no side in conflict is **0**.

    $ stardis -V 2 -M merge_conflict.txt -d > merge_conflict.vtk
    warning: Merge conflicts found reading file cube_1.stl (12 triangles).


Dump geometries with invalid connections
========================================

In each of the following examples there is some invalid connection or invalid
boundary condition. As a consequence, the field **Property_conflict** is present
in resut files and gives the error code for each triangle. The value associated
to triangles with no conflict is **0**.


H_BOUNDARY_FOR_FLUID between 2 defined media
--------------------------------------------

Triangles that have property H_BOUNDARY_FOR_FLUID and have a medium on their
two sides have an error value of **1**.

    $ stardis -V 2 -M valid_material.txt -M hf_2def_bc.txt -d > hf_2def_error.vtk
    warning: Properties conflicts found in the model (2 triangles).
      

H_BOUNDARY_FOR_FLUID between 2 undefined media
----------------------------------------------

The triangles that have property H_BOUNDARY_FOR_FLUID and have no defined
medium on their two sides have an error value of **2**. Additionaly, as the
central enclosures' medium is undefined, this enclosure is invalid.

    $ stardis -V 2 -M material_undef_center.txt -M hf_2undef_bc.txt -d > hf_2undef_error.vtk
    warning: Properties conflicts found in the model (8 triangles).
    warning: Found 1 invalid enclosure(s).

      
H_BOUNDARY_FOR_FLUID as a boundary for a solid
----------------------------------------------

Triangles that have property H_BOUNDARY_FOR_FLUID as a boundary for a solid
have an error value of **3**.

    $ stardis -V 2 -M valid_material.txt -M hf_solid_bc.txt -d > hf_solid_error.vtk
    warning: Properties conflicts found in the model (20 triangles).
      

H_BOUNDARY_FOR_SOLID between 2 defined media
--------------------------------------------

Triangles that have property H_BOUNDARY_FOR_SOLID and have a medium on their
two sides have an error value of **4**.

    $ stardis -V 2 -M valid_material.txt -M hs_2def_bc.txt -d > hs_2def_error.vtk
    warning: Properties conflicts found in the model (2 triangles).
      

H_BOUNDARY_FOR_SOLID between 2 undefined media
----------------------------------------------

The triangles that have property H_BOUNDARY_FOR_SOLID and have no defined
medium on their two sides have an error value of **5**. Additionaly, as the
central enclosures' medium is undefined, this enclosure is invalid.

    $ stardis -V 2 -M material_undef_center.txt -M hs_2undef_bc.txt -d > hs_2undef_error.vtk
    warning: Properties conflicts found in the model (8 triangles).
    warning: Found 1 invalid enclosure(s).

      
H_BOUNDARY_FOR_SOLID as a boundary for a fluid
----------------------------------------------

Triangles that have property H_BOUNDARY_FOR_SOLID as a boundary for a fluid
have an error value of **6**.

    $ stardis -V 2 -M valid_material.txt -M hs_fluid_bc.txt -d > hs_fluid_error.vtk
    warning: Properties conflicts found in the model (8 triangles).


T_BOUNDARY_FOR_FLUID between 2 defined media
--------------------------------------------

Triangles that have property T_BOUNDARY_FOR_FLUID and have a medium on their
two sides have an error value of **7**.

    $ stardis -V 2 -M valid_material.txt -M tf_2def_bc.txt -d > tf_2def_error.vtk
    warning: Properties conflicts found in the model (2 triangles).
      

T_BOUNDARY_FOR_FLUID between 2 undefined media
----------------------------------------------

The triangles that have property T_BOUNDARY_FOR_FLUID and have no defined
medium on their two sides have an error value of **8**. Additionaly, as the
central enclosures' medium is undefined, this enclosure is invalid.

    $ stardis -V 2 -M material_undef_center.txt -M tf_2undef_bc.txt -d > tf_2undef_error.vtk
    warning: Properties conflicts found in the model (8 triangles).
    warning: Found 1 invalid enclosure(s).

      
T_BOUNDARY_FOR_FLUID as a boundary for a solid
----------------------------------------------

Triangles that have property T_BOUNDARY_FOR_FLUID as a boundary for a solid
have an error value of **9**.

    $ stardis -V 2 -M valid_material.txt -M tf_solid_bc.txt -d > tf_solid_error.vtk
    warning: Properties conflicts found in the model (20 triangles).
      
      
T_BOUNDARY_FOR_SOLID between 2 defined media
--------------------------------------------

Triangles that have property T_BOUNDARY_FOR_SOLID and have a medium on their
two sides have an error value of **10**.

    $ stardis -V 2 -M valid_material.txt -M ts_2def_bc.txt -d > ts_2def_error.vtk
    warning: Properties conflicts found in the model (2 triangles).
      

T_BOUNDARY_FOR_SOLID between 2 undefined media
----------------------------------------------

The triangles that have property T_BOUNDARY_FOR_SOLID and have no defined
medium on their two sides have an error value of **11**. Additionaly, as the
central enclosures' medium is undefined, this enclosure is invalid.

    $ stardis -V 2 -M material_undef_center.txt -M ts_2undef_bc.txt -d > ts_2undef_error.vtk
    warning: Properties conflicts found in the model (8 triangles).
    warning: Found 1 invalid enclosure(s).

      
T_BOUNDARY_FOR_SOLID as a boundary for a fluid
----------------------------------------------

Triangles that have property T_BOUNDARY_FOR_SOLID as a boundary for a fluid
have an error value of **12**.

    $ stardis -V 2 -M valid_material.txt -M ts_fluid_bc.txt -d > ts_fluid_error.vtk
    warning: Properties conflicts found in the model (8 triangles).
      
      
F_BOUNDARY_FOR_SOLID between 2 defined media
--------------------------------------------

Triangles that have property F_BOUNDARY_FOR_SOLID and have a medium on their
two sides have an error value of **13**.

    $ stardis -V 2 -M valid_material.txt -M fs_2def_bc.txt -d > fs_2def_error.vtk
    warning: Properties conflicts found in the model (2 triangles).
      

F_BOUNDARY_FOR_SOLID between 2 undefined media
----------------------------------------------

The triangles that have property F_BOUNDARY_FOR_SOLID and have no defined
medium on their two sides have an error value of **14**. Additionaly, as the
central enclosures' medium is undefined, this enclosure is invalid.

    $ stardis -V 2 -M material_undef_center.txt -M fs_2undef_bc.txt -d > fs_2undef_error.vtk
    warning: Properties conflicts found in the model (8 triangles).
    warning: Found 1 invalid enclosure(s).

      
F_BOUNDARY_FOR_SOLID as a boundary for a fluid
----------------------------------------------

Triangles that have property F_BOUNDARY_FOR_SOLID as a boundary for a fluid
have an error value of **15**.

    $ stardis -V 2 -M valid_material.txt -M fs_fluid_bc.txt -d > fs_fluid_error.vtk
    warning: Properties conflicts found in the model (8 triangles).

      
SOLID_FLUID_CONNECTION between two solids
-----------------------------------------

Triangles that have property SOLID_FLUID_CONNECTION and have a solid on
their two sides have an error value of **16**.

    $ stardis -V 2 -M 2solids.txt -M sf_2s_bc.txt -d > sf_2s_error.vtk
    warning: Properties conflicts found in the model (2 triangles).
      
      
SOLID_FLUID_CONNECTION between two fluids
-----------------------------------------

Triangles that have property SOLID_FLUID_CONNECTION and have a fluid on
their two sides have an error value of **17**.

    $ stardis -V 2 -M 2fluids.txt -M sf_2f_bc.txt -d > sf_2f_error.vtk
    warning: Properties conflicts found in the model (2 triangles).
      
      
SOLID_FLUID_CONNECTION used as a boundary
-----------------------------------------

The triangles that have property SOLID_FLUID_CONNECTION and have only a single
side with defined medium have an error value of **18**.

    $ stardis -V 2 -M valid_material.txt -M sf_bound_bc.txt -d > sf_bound_error.vtk
    warning: Properties conflicts found in the model (8 triangles).
      
      
SOLID_FLUID_CONNECTION between 2 undefined media
------------------------------------------------

The triangles that have property F_BOUNDARY_FOR_SOLID and have no defined
medium on their two sides have an error value of **19**. Additionaly, as the
central enclosures' medium is undefined, this enclosure is invalid.

    $ stardis -V 2 -M material_undef_center.txt -M sf_2undef_bc.txt -d > sf_2undef_error.vtk
    warning: Properties conflicts found in the model (8 triangles).
    warning: Found 1 invalid enclosure(s).

Undefined connection between two fluids
---------------------------------------

Triangles that have undefined interface property and have a fluid on
their two sides have an error value of **20**.

    $ stardis -V 2 -M 2fluids.txt -M undef_2f_bc.txt -d > undef_2f_error.vtk
    warning: Properties conflicts found in the model (2 triangles).
      
      
Undefined connection between a solid and a fluid
------------------------------------------------

Triangles that have undefined interface property and have a fluid on one
side and a solid on the other have an error value of **21**.

    $ stardis -V 2 -M valid_material.txt -M undef_sf_bc.txt -d > undef_sf_error.vtk
    warning: Properties conflicts found in the model (4 triangles).
      
      
Undefined boundary for a fluid
------------------------------

Triangles that have undefined interface property and have a fluid on one
side and no efined medium on the other have an error value of **22**.

    $ stardis -V 2 -M valid_material.txt -M undef_uf_bc.txt -d > undef_uf_error.vtk
    warning: Properties conflicts found in the model (8 triangles).
      
      
Undefined boundary for a solid
------------------------------

Triangles that have undefined interface property and have a solid on one
side and no efined medium on the other have an error value of **23**.

    $ stardis -V 2 -M valid_material.txt -M undef_us_bc.txt -d > undef_us_error.vtk
    warning: Properties conflicts found in the model (20 triangles).

Invalid part of a compute surface
---------------------------------

Triangles that have been added as a compute surface through the **-s**,
**-S**, or **-F** command line option, but that where not part of the
geometry as defined through the **-M** command line option have an error
value of **24**.
      
    $ stardis -V 2 -M material_undef_center.txt -M ts_undef_center_bc.txt -s cube_2_ext.stl -d > surf_region_error.vtk
    warning: Invalid compute region defined by file 'cube_2_ext.stl'.
    warning: The file contains 8 triangles not in the model.
    warning: Found 1 invalid enclosure(s).
