
  SOLID     AL   237   2700   890    auto     300  UNKNOWN     0   FRONT mesh1.stl
  SOLID     SI   148   2330   700    auto     300  UNKNOWN     0   FRONT mesh2.stl
  SOLID   SIPw   148   2330   700    auto     300  UNKNOWN   1e8   FRONT mesh3.stl
  
  H_BOUNDARY_FOR_SOLID   CL  300     0.5     0.3   250  320  cl.stl
  
  SCALE 0.001 # model is in mm
  TRAD 280 300
