Test of green functions
=======================

    /* The scene is composed of a solid cube enclosed between two fluid regions.
     * No temperature inside the scene is known, and all the frontiers of the scene,
     * either solid or fluid, are adiabatic except the leftmost and rightmost sides
     * whose temperature is imposed. The solid cube volume is a heat source.
     *
     *                               (1, 1, 1)
     *                 +------+----------+------+ (1.5,1,1)
     *                /'     /##########/'     /|
     *               +------+----------+------+ |
     *               | '    |##########|*'    | | Tright
     *               |E=1  E|##########|*'E   |E=1
     *         Tleft | '  hc|### Pw ###|*'hc  | |
     *               | +....|##########|*+....|.+
     *               |/     |##########|/     |/
     *  (-1.5,-1,-1) +------+----------+------+
     *                  (-1,-1,-1)
     */


The model used here is the same used in Compute/conducto_radiative.

Probe computation with Green ASCII output.

    $ stardis -V 2 -M model.txt -p 0,0,0 -n 1000 -g > probe_green.txt
      
Same computation, more samples, binary output.

    $ stardis -V 2 -M model.txt -p 0,0,0 -n 20000 -G probe.green
      
    $ sgreen -V 2 -g probe.green -s green.html
      
    $ sgreen -V 2 -g probe.green -a settings.txt

    warning: In file 'apply.txt': attempt to change unused variable 'LEFT.T':
    warning: LEFT.T=100
    380.722 0.357044
    812.871 2.873
    837.816 2.88053
    355.777 0.289433
    355.777 0.289433
      
    $ sgreen -e -g probe.green -a settings.txt

    380.722 K +/- 0.357044 ; SOL.VP = 10    RMOST.T = 360
    812.871 K +/- 2.873 ; SOL.VP = 100    RMOST.T = 310
    837.816 K +/- 2.88053 ; SOL.VP = 100    RMOST.T = 360
    355.777 K +/- 0.289433 ; SOL.VP = 10    RMOST.T = 310
    355.777 K +/- 0.289433 ; LEFT.T=100

Check the SOL.VP = 100 RMOST.T = 360 configuration

    $ stardis -V 2 -M model_100_360.txt -p 0,0,0 -n 20000
    
    836.503 2.89168 0 20000