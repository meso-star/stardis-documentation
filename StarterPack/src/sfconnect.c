static const char copyright_notice[] =
"Copyright (C) 2021-2022 |Meso|Star> (contact@meso-star.com).";

static const char license_short[] =
"Licensed under GPLv3, see <http://www.gnu.org/licenses/>.";

static const char license_text[] =
"This program is free software: you can redistribute it and/or modify\n\
it under the terms of the GNU General Public License as published by\n\
the Free Software Foundation, either version 3 of the License, or\n\
(at your option) any later version.\n\
This program is distributed in the hope that it will be useful,\n\
but WITHOUT ANY WARRANTY; without even the implied warranty of\n\
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the\n\
GNU General Public License for more details.\n\
You should have received a copy of the GNU General Public License\n\
along with this program. If not, see <http://www.gnu.org/licenses/>.";

/* Build:
 * gcc -shared -g -I ${stardis_path}/include -o libsfconnect.so -fPIC sfconnect.c
 */

#include <stardis/stardis-prog-properties.h>

#include <float.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#define CHK_ARG(Name) \
  if(argc <= idx) {\
    fprintf(stderr,\
       "Solid-Fluid connection '%s': invalid data (missing token '" Name "')\n",\
       connect->name);\
    goto error;\
  } else arg = argv[idx++]

struct my_sfconnect {
  char name[128];
  double emissivity, alpha, hc, ref_temp;
};

void
stardis_release_data
  (void* my_sfconnect)
{
  assert(my_sfconnect);
  free(my_sfconnect);
}

void*
stardis_create_data
  (const struct stardis_description_create_context* ctx,
   void* data,
   size_t argc,
   char* argv[])
{
  struct my_sfconnect* connect = NULL;
  char *arg, *end;
  size_t idx = 0;

  (void)data;

  if(!argv || ! ctx) goto error;
  connect = malloc(sizeof(*connect));
  if(!connect) goto error;

  strncpy(connect->name, ctx->name, 127);

  /* argv[0] = reference temperature */
  CHK_ARG("reference temperature");
  connect->ref_temp = strtod(arg, &end);
  if(connect->ref_temp < 0 || arg == end) {
    fprintf(stderr,
       "Solid-Fluid connection '%s': invalid reference temperature: %s\n",
       connect->name, arg);
    goto error;
  }
  /* argv[1] = reference temperature */
  CHK_ARG("reference temperature");
  connect->emissivity = strtod(arg, &end);
  if(connect->emissivity < 0 || connect->emissivity > 1 || arg == end) {
    fprintf(stderr,
       "Solid-Fluid connection '%s': invalid emissivity: %s\n",
       connect->name, arg);
    goto error;
  }
  /* argv[2] = specular fraction */
  CHK_ARG("specular fraction");
  connect->alpha = strtod(arg, &end);
  if(connect->alpha < 0 || connect->alpha > 1 || arg == end) {
    fprintf(stderr,
       "Solid-Fluid connection '%s': invalid specular fraction: %s\n",
       connect->name, arg);
    goto error;
  }
  /* argv[3] = convection coefficient */
  CHK_ARG("convection coefficient");
  connect->hc = strtod(arg, &end);
  if(connect->hc < 0 || arg == end) {
    fprintf(stderr,
       "Solid-Fluid connection '%s': invalid convection coefficient: %s\n",
       connect->name, arg);
    goto error;
  }
  if(argc > idx) {
    fprintf(stderr, "Solid-Fluid connection '%s': trailing arguments:\n",
      connect->name);
    for( ; idx < argc; idx++) fprintf(stderr, "%s\n", argv[idx]);
    goto error;
  }

end:
  return connect;
error:
  if(connect) stardis_release_data(connect);
  connect = NULL;
  goto end;
}

const char*
get_copyright_notice
  (void* data)
{
  (void)data;
  return copyright_notice;
}

const char*
get_license_short
  (void* data)
{
  (void)data;
  return license_short;
}

const char*
get_license_text
  (void* data)
{
  (void)data;
  return license_text;
}

double
stardis_emissivity
  (const struct stardis_interface_fragment* frag,
   void* data)
{
  struct my_sfconnect* connect = data;
  (void)frag;
  return connect->emissivity;
}

double
stardis_specular_fraction
  (const struct stardis_interface_fragment* frag,
   void* data)
{
  struct my_sfconnect* connect = data;
  (void)frag;
  return connect->alpha;
}

double
stardis_convection_coefficient
  (const struct stardis_interface_fragment* frag,
   void* data)
{
  struct my_sfconnect* connect = data;
  (void)frag;
  return connect->hc;
}

double
stardis_max_convection_coefficient
  (void* data)
{
  struct my_sfconnect* connect = data;
  return connect->hc;
}

double*
stardis_t_range
  (void* data,
   double range[2])
{
  struct my_sfconnect* connect = data;
  range[0] = fmin(range[0], connect->ref_temp);
  range[1] = fmax(range[1], connect->ref_temp);
  return range;
}

double
stardis_reference_temperature
  (const struct stardis_interface_fragment* frag,
   void* data)
{
  struct my_sfconnect* connect = data;
  (void)frag;
  return connect->ref_temp;
}
