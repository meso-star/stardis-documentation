Test test_sdis_solve_boundary_flux  from stardis-solver project
===============================================================

    /*
     * The scene is composed of a solid cube whose temperature is unknown.
     * The convection coefficient with the surrounding fluid is null excepted for
     * the +X face whose value is 'H'. The Temperature T of the -X face is fixed
     * to Tb. The ambiant radiative temperature is 0 excepted for the +X face
     * whose value is 'Trad'.
     * This test computes temperature and fluxes on the X faces and check that
     * they are equal to:
     *
     *    T(+X) = (H*Tf + Hrad*Trad + LAMBDA/A * Tb) / (H+Hrad+LAMBDA/A)
     *         with Hrad = 4 * BOLTZMANN_CONSTANT * Tref^3 * epsilon
     *    T(-X) = Tb
     *
     *    CF = H * (T - Tf)
     *    RF = Hrad * (T - Trad)
     *    TF = CF + RF
     *
     * with Tf the temperature of the surrounding fluid, lambda the conductivity of
     * the cube and A the size of the cube/square, i.e. 1.
     *
     *
     *                ///////(1,1,1)
     *               +-------+
     *              /'      /|    _\       <-----
     *             +-------+ |   / / H,Tf  <----- Trad
     *            Tb +.....|.+   \__/      <-----
     *             |,      |/
     *             +-------+
     *       (0,0,0)///////
     */


    $  stardis -V 3 -M model.txt -P 1,0.6,0.3,inf -e
    output: Use 8 threads.
    output: Dummy fluid created: (it is medium 1)
    output: External fluid created: T=300 (it is medium 2)
    output: Scaling factor is 1
    output: Trad is 300, Trad reference is 300
    output: Auto delta for solid CUBE set to 0.0277778
    output: Description 0: Solid 'CUBE': lambda=0.1 rho=25 cp=2 delta=0.0277778 Tinit=300 (it is medium 0)
    output: Description 1: T boundary for solid' TEMP': T=0 (using medium 1 as external medium)
    output: Description 2: F boundary for SOLID 'ADIA': flux=0 (using medium 1 as external medium)
    output: Description 3: H boundary for solid 'HdT': emissivity=1 specular_fraction=0 hc=0.5 T=300 (using medium 2 as external medium)
    output: Probe is on primitive 6, uv = (0.3, 0.1), not moved.
    output: Solving probe boundary temperature: 100%
    Boundary temperature at [1, 0.6, 0.3] at t=inf = 295.71 K +/- 0.356174
    #failures: 0/10000

Expected T=295.54 °K
      
    $ stardis -V 2 -M model.txt -s right_bc.stl -e
    Temperature at boundary 'right_bc.stl' at t=inf = 294.9 K +/- 0.387813
    #failures: 0/10000

Expected T=295.54 °K
      
    $ stardis -V 2 -M model.txt -F right_bc.stl,inf -e -n 200000
    Temperature at boundary 'right_bc.stl' at t=inf = 295.457 K +/- 0.081927
    Convective flux at boundary 'right_bc.stl' at t=inf = -2.27175 W +/- 0.0409635
    Radiative flux at boundary 'right_bc.stl' at t=inf = -27.8206 W +/- 0.501653
    Imposed flux at boundary 'right_bc.stl' at t=inf = 0 W +/- 0
    Total flux Flux at boundary 'right_bc.stl' at t=inf = -30.0924 W +/- 0.542616
    #failures: 0/200000

Expected T=295.54 °K, Convective flux=-2.23 W·m-2, Radiative flux=-27.32 W·m-2, Imposed flux=0 W.m-2, and Total flux=-29.5538 W·m-2
      
    $ stardis -V 2 -M model.txt -F right_bc.stl,inf
    295.11 0.37988 -2.445 0.18994 -29.9423 2.32607 0 0 -32.3873 2.51601 0 10000

Expected T=295.54 °K, Convective flux=-2.23 W·m-2, Radiative flux=-27.32 W·m-2, Imposed flux=0 W.m-2, and Total flux=-29.5538 W·m-2

    $ stardis -V 2 -M model.txt -P 0.5,0.2,0.8,inf
    error: Probe moved to (0.5, 0.2, 1), primitive 5, uv = (0.2, 0.5).
    error: Move is 7.2 delta long. Use -p instead of -P.
    error: stardis_compute: computation failed!
    error: No computation possible.

Failure expected

    $ stardis -V 2 -M model.txt -p 0.5,0.2,0.8,inf
    146.55 1.4996 0 10000

Expected T=147.77°K

    $ stardis -V 2 -M model.txt -s left_bc.stl,inf -e
    Temperature at boundary 'left_bc.stl' at t=inf = 0 K +/- 0
    #failures: 0/10000

Expected T=0 °K
      
    $ stardis -V 2 -M model.txt -P 0,0.3,0.4 -e
    Boundary temperature at [0, 0.3, 0.4] at t=inf = 0 K +/- 0
    #failures: 0/10000
    
Expected T=0 °K
