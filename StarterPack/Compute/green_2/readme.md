Test of green functions
=======================

    /* The scene is a derivative of the scene in the green_1 directory in which 
     * a few small cubes have been added to make every possible case happen in 
     * the summary :
     * - An external solid cube with known temperature that will not be sampled
     * - An very-small solid cube in the central cube with known temperature that
     *   will be sampled
     *
     *                               (1, 1, 1)
     *                 +------+----------+------+ (1.5,1,1)
     *                /'     /##########/'     /|                  +----+
     *               +------+----------+------+ |                 /'   /|
     *               | '    |##########|*'    | | Tright         +----+ |
     *               |E=1  E|#####[]###|*'E   |E=1               | +..|.+
     *         Tleft | '  hc|### Pw ###|*'hc  | |                |/   |/
     *               | +....|##########|*+....|.+                +----+
     *               |/     |##########|/     |/
     *  (-1.5,-1,-1) +------+----------+------+
     *                  (-1,-1,-1)
     */


The model used here is the same used in Compute/conducto_radiative.

Probe computation with Green ASCII output.

    $ stardis -V 2 -M model.txt -p 0,0,0 -n 1000 -g > probe_green.txt

Same computation, more samples, binary output.

    $ stardis -V 2 -M model.txt -p 0,0,0 -n 20000 -G probe.green
      
    $ sgreen -V 2 -g probe.green -s green.html
      
    $ sgreen -V 2 -g probe.green -a settings.txt
    
    warning: In file 'settings.txt': attempt to change unused variable 'LEFT.T':
    warning: LEFT.T=100
    355.699 0.288629
    380.604 0.356012
    812.157 2.86576
    837.062 2.87284
    455.319 0.795017
    355.699 0.288629

    $ sgreen -e -g probe.green -a settings.txt
    
    355.699 K +/- 0.288629 ; SOL.VP = 10    RMOST.T = 310
    380.604 K +/- 0.356012 ; SOL.VP = 10    RMOST.T = 360
    812.157 K +/- 2.86576 ; SOL.VP = 100    RMOST.T = 310
    837.062 K +/- 2.87284 ; SOL.VP = 100    RMOST.T = 360
    455.319 K +/- 0.795017 ; SOL.VP = 10    RMOST.T = 510
    355.699 K +/- 0.288629 ; LEFT.T=100

Check the SOL.VP = 100 RMOST.T = 360 configuration

    $ stardis -V 2 -M model_100_360.txt -p 0,0,0 -n 20000

    836.576 2.88821 0 20000
