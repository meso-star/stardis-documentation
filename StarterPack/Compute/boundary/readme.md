Test test_sdis_solve_boundary from stardis-solver project
=========================================================

    /*
     * The scene is composed of a solid cube whose temperature is unknown.
     * The convection coefficient with the surrounding fluid is null excepted for
     * the +X face whose value is 'H'. The Temperature of the -X face is fixed to
     * Tl. The temperature Tr on the +X face is:
     *
     *    Tr = (H*Tf + LAMBDA/A * Tl) / (H+LAMBDA/A); with A=1, the size of the cube
     *
     *     and LAMBDA the conductivity of the solid.
     *
     *
     * The temperature at X=x T(x) = k Tr + (1 - k) Tl, where k in [0 1] is the
     * normalized position
     *
     *       ///// (1,1,1)
     *       +-------+
     *      /'      /|    _\
     *     +-------+ |   / /   Tf
     *    Tl +.....|.+   \__/
     *     |,      |/
     *     +-------+
     * (0,0,0) /////
     */


    $ stardis -M model.txt -P 1,0.6,0.3 -e
    Boundary temperature at [1, 0.6, 0.3] at t=inf = 308.361 K +/- 0.0370185
    #failures: 0/10000

Expected T=308.33°K

    $ stardis -V 2 -M model.txt -s right_bc.stl
    308.348 0.0371361 0 10000

Expected T=308.33°K

    $ stardis -V 2 -M model.txt -P 0.5,0.2,0.8 -e
    error: Probe moved to (0.5, 0.2, 1), primitive 5, uv = (0.2, 0.5).
    error: Move is 4 delta long. Use -p instead of -P.
    error: stardis_compute: computation failed!
    error: No computation possible.

Failure expected

    $ stardis -V 2 -M model.txt -p 0.5,0.2,0.8 -e
    Temperature at [0.5, 0.2, 0.8] at t=inf = 304.161 K +/- 0.0492911
    #failures: 0/10000

Expected T=304.17°K

	$ stardis -V 2 -M model.txt -P 0.5,0.2,0.98 -e
	Boundary temperature at [0.5, 0.2, 1] at t=inf = 304.196 K +/- 0.0493494
	#failures: 0/10000

Expected T=304.17°K
