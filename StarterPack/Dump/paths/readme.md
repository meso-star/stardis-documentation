Dump paths
==========

Compute the mean temperature in medium **PUCE** by sampling 10
heat paths and write these paths in VTK files.

    $ stardis -V 2 -M module.txt -m PUCE,INF -n 10 -D all,path_ -e
    Temperature in medium PUCE at t=inf = 378.293 K +/- 9.16519
    #failures: 0/10

    $ ls path_*.vtk
    path_00000000.vtk  path_00000002.vtk  path_00000004.vtk  path_00000006.vtk  path_00000008.vtk
    path_00000001.vtk  path_00000003.vtk  path_00000005.vtk  path_00000007.vtk  path_00000009.vtk




