#!/bin/sh
# Copyright (C) 2018-2020 |Meso|Star>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

compat()
{
  mc1=$(echo "$1" | cut -d ' ' -f 1)
  std1=$(echo "$1" | cut -d ' ' -f 2)
  mc2=$(echo "$2" | cut -d ' ' -f 1)
  std2=$(echo "$2" | cut -d ' ' -f 2)
  echo "MC result #1: $mc1 +/- $std1"
  echo "MC result #2: $mc2 +/- $std2"

  diff=$(echo "define abs(x) { if (x>=0) return(x); scale=9; { return(-x); } } ; abs($mc1 - $mc2)" | bc)
  sigma=$(echo "scale=9; 2 * ($std1 + $std2)" | bc)

  ok=$(echo "$sigma >= $diff" | bc)
  if [ "$ok" -ne 1 ]
  then
    echo "Error: Incompatible results  $mc1 +/- $std1 VS $mc2 +/- $std2" >&2
    exit 1
  fi
  # check that no sample failed for mc1 (last part of result: #failed #success)
  failed=$(echo "$1" | cut -d ' ' -f 3)
  if [ "$failed" -ne 0 ]
  then
    echo "Error: $failed sample(s) failed" >&2
    exit 1
  fi
  # check that no sample failed for mc1 (last part of result: #failed #success)
  failed=$(echo "$2" | cut -d ' ' -f 3)
  if [ "$failed" -ne 0 ]
  then
    echo "Error: $failed sample(s) failed" >&2
    exit 1
  fi
} 

# run two commands and check result(s) compatibility
# success expected
if ! result1=$( $1 )
then
  echo "Error: Command failed!" >&2
  exit 1
fi
re='^[0-9 .eEgG+-]+$'
if ! echo "$result1" | grep -Eq "$re" ; then
  echo "Error: result does not start with a number: $result1" >&2
  echo "Error: don't use the option -e with stardis" >&2
  exit 1
fi

if ! result2=$( $2 )
then
  echo "Error: Command failed!" >&2
  exit 1
fi
re='^[0-9 .eEgG+-]+$'
if ! echo "$result2" | grep -Eq "$re" ; then
  echo "Error: result does not start with a number: $result2" >&2
  echo "Error: don't use the option -e with stardis" >&2
  exit 1
fi
compat "$result1" "$result2"
echo "Message: Results are compatible"
