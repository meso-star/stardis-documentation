# Stardis-docs

## Overview

Stardis-docs is a collection of additional documents for stardis learners
and users.


## License

This work is made available to you under the Creative Attribution
ShareAlike 4.0 International license. See
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)
for more information.

## Release notes:

### v0.1

- Include a Starter Pack with a lot of examples.
- Successfully tested with stardis 0.5.0.
