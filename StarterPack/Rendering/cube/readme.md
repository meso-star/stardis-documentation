Rendering of a cube
===================

    /*
     * The scene is composed of a solid cube whose temperature is unknown.
     * The convection coefficient with the surrounding fluid is 'H' excepted for
     * the -X face whose temperature is fixed to Tb.
     *
     *             (1,1,1)
     *       +-------+
     *      /'      /|    _\              ___
     *     +-------+ |   / /   Tf       [|   |=|     
     *    Tb +.....|.+   \__/            |___|
     *     |,      |/                     /|\ 
     *     +-------+                     / | \
     * (0,0,0)
     *
     * The camera is at the right of the cube, looking at its right side.
     */


Frontal view of the front (hot) side :

    $ stardis -V 2 -M material.txt -M bc.txt -R pos=2,0.5,0.5:tgt=0.5,0.5,0.5:file=image_1.ht
      
Three-quarter view of the front (hot) side :

    $ stardis -V 2 -M material.txt -M bc.txt -R pos=2,1.3,1.3:tgt=0.5,0.5,0.5:file=image2.ht
      
Three-quarter view of the back (cold) side:

    $ stardis -V 2 -M material.txt -M bc.txt -R pos=-1,1.3,1.3:tgt=0.5,0.5,0.5:fmt=vtk > image3.vtk
