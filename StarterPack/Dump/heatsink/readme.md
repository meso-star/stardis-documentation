Dump of the model without errors
================================

    $ stardis -V 2 -M material.txt -M bc.txt -d > no_conflict.vtk
      
Neither **Merge_conflicts** or **Property_conflicts** fields are available.
      
Dump of the model with merge conflicts
======================================

    $ stardis -V 2 -M material_merge_conflicts.txt -M bc.txt -d > merge_conflicts.vtk
    warning: Merge conflicts found in the the model (276 triangles).

      
Field **Merge_conflicts** is available, but **Property_conflicts** is not.
Possible values for merge conflicts are:
- 0 : No merge conflict for this triangle.
- 1 : A merge conflict occured for this triangle.
      
Dump of the model with property conflicts
=========================================

    $ stardis -V 2 -M material_property_conflicts.txt -M bc.txt -d > property_conflicts.vtk
    warning: Properties conflicts found in the the model (2 triangles).
    warning: Found 1 invalid enclosure(s).

      
Field **Property_conflicts** is available, but **Merge_conflicts** is not.
Possible values for property conflicts are:
- 0 : No property conflict for this triangle.
- 1-24 : A property conflict occured for this triangle.
