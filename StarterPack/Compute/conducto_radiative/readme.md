Test test_sdis_conducto_radiative from stardis-solver project
=============================================================

    /* The scene is composed of a solid cube whose temperature is unknown. The cube
     * faces on +/-X are in contact with a fluid and their convection coefficient
     * is null while their emissivity is 1. The left and right fluids are enclosed
     * by surfaces whose emissivity are null excepted for the faces orthogonal to
     * the X axis that are fully emissive and whose temperature is known. The
     * medium that surrounds the solid cube and the 2 fluids is a solid with a null
     * conductivity.
     *
     *                               (1, 1, 1)
     *                 +------+----------+------+ (1.5,1,1)
     *                /'     /##########/'     /|
     *               +------+----------+------+ |
     *               | '    |##########|*'    | | 310K
     *               | '    |##########|*'    | |
     *          300K | ' E=1|##########|*'E=1 | |
     *               | +....|##########|*+....|.+
     *               |/     |##########|/     |/
     *  (-1.5,-1,-1) +------+----------+------+
     *                  (-1,-1,-1)
     *
     * If at time=INF:
     * - inside the solid, T(x,y,z) = u * 310 + (1-u) * 300; u = (1 + x) / 2
     * - in fluids, as hc=0 everywhere, T(x,y,z) = Tinit (true at any time)
     */


The original test_sdis_conducto_radiative was not meant to compute probes
out of the central solid region and therefore cannot (no initial temperature
provided). Here, as we provided an initial temperature for fluids, it is
possible to compute probes in fluids.

    $ stardis -V 2 -M model.txt -p 0,0,0,inf
    305.011 0.0499999 0 10000

Expected T=305°K
      
    $ stardis -V 2 -M model.txt -p 0.5,0.9,-0.9 -e
    Temperature at [0.5, 0.9, -0.9] at t=inf = 307.445 K +/- 0.0436142
    #failures: 0/10000

Expected T=307.5°K

    $ stardis -V 2 -M model.txt -p 1.25,0,0,INF
    warning: Probe is in fluid 'RF': computing fluid temperature, not using a specific position.
    300 0 0 10000

Expected T=300°K
