Test of programmed connections
==============================

    /* The scene is a derivative of the scene in the conducto_radiative
     * directory to which a few small cubes have been added to make every
     * possible case happen in the summary :
     * - An external solid cube with known temperature that will not be sampled
     * - A very-small solid cube in the central cube with known temperature that
     *   will be sampled and a contact resistance at its interface.
     *
     *                               (1, 1, 1)
     *                 +------+----------+------+ (1.5,1,1)
     *                /'     /##########/'     /|                  +----+
     *               +------+----------+------+ |                 /'   /|
     *               | '    |##########|*'    | | Tright         +----+ |
     *               |E=1  E|#####[]###|*'E   |E=1               | +..|.+
     *         Tleft | '  hc|### Pw ###|*'hc  | |                |/   |/
     *               | +....|##########|*+....|.+                +----+
     *               |/     |##########|/     |/
     *  (-1.5,-1,-1) +------+----------+------+
     *                  (-1,-1,-1)
     */


No analytic result is know.
Check that model and model_prog return the same results:

    for t in 0 10 100 1000
    do
      diff <(stardis -M model.txt -n 1000 -p 0,0,0,$t) <(stardis -M model_prog.txt -n 1000 -p 0,0,0,$t)
      diff <(stardis -M model.txt -n 1000 -p 0.5,0.9,-0.9,$t) <(stardis -M model_prog.txt -n 1000 -p 0.5,0.9,-0.9,$t)
    done

